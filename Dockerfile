FROM ubuntu:16.04

LABEL maintainer="Martin"

COPY ./hello-go /hello-go/hello-go
COPY ./static /hello-go/static
COPY ./template /hello-go/template

EXPOSE 8080

WORKDIR /hello-go
CMD ["./hello-go"]
