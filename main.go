package main

import (
	"html/template"
	"log"
	"net/http"
	"path/filepath"
)

var (
	tmpls = make(map[string]*template.Template)
)

func renderTemplate(name string, w http.ResponseWriter) {
	tmpl, ok := tmpls[name]
	if !ok {
		tmpl, err := template.ParseFiles(
			filepath.Join("template", "layout.html"),
			filepath.Join("template", name+".html"),
		)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		tmpls[name] = tmpl
	}
	tmpl.ExecuteTemplate(w, "layout", nil)
}

func main() {

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	http.HandleFunc("/dinosaur/", func(w http.ResponseWriter, r *http.Request) {
		renderTemplate("dinosaur", w)
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		renderTemplate("index", w)
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}
